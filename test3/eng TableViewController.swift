//
//  eng TableViewController.swift
//  test3
//
//  Created by apple on 2017/4/14.
//  Copyright © 2017年 apple. All rights reserved.
//

import UIKit
import AVFoundation
import  CoreData

class eng_TableViewController: UITableViewController {
    var mycontext : NSManagedObjectContext!
    var english: [EnglishMO] = []
    
    func getContext () -> NSManagedObjectContext{
    let appDelegate = UIApplication.shared.delegate as! AppDelegate
        return appDelegate.persistentContainer.viewContext
    }
    
    func storeEnglish(){
    let context = getContext()
        var english:EnglishMO!
        english = EnglishMO(context: getContext())
        english.name = "earnings"
        english.cht = "薪水"
        english.speak = "ˋɝnɪŋz"
        if let englishImage = UIImage(named:"earnings.jpg"){
        if let imageData = UIImageJPEGRepresentation(englishImage, 1.0)
        {
            english.picture = NSData(data:imageData )
            
            }
        }
        
        do {
        try context.save()
            print("success")
            
        } catch {
        print(error)
        
        }
        
    }
    func getWord() {
        let request:NSFetchRequest<EnglishMO> = EnglishMO.fetchRequest()
        let context = getContext()
        do {
            english = try context.fetch(request)
            print(english.count)
            for word in english {
                print(word.name!)
                print(word.cht!)
                print(word.speak!)
                
            }
        } catch  {
            print(error)
        }
    }
    func delCoreData() {
        let request:NSFetchRequest<EnglishMO> = EnglishMO.fetchRequest()
        let context = getContext()
        do {
            english = try context.fetch(request)
            print(english.count)
            for word in english {
                print(word.name!)
                print(word.cht!)
                print(word.speak!)
                
            }
        } catch  {
            print(error)
        }
    }

    var englishs : [English] = [
        
        English(name:"ice cream",cht:"冰淇淋", speak:"ˌaɪs ˈkrim", picture:"9.jpg"),
        English(name:"icon",cht:"圖標", speak:"īˌkän", picture:"19.jpg"),
        English(name:"ideal",cht:"理想;志向", speak:"īˈdē(ə)l", picture:"5.jpg"),
        English(name:"ident",cht:"識別", speak:"ˈīdent", picture:"14.jpg"),
        English(name:"idea",cht:"想法", speak:"īdz", picture:"7.png"),
        English(name:"illness",cht:"疾病", speak:"ˈilnəs", picture:"1.jpg"),
        English(name:"idol",cht:"偶像", speak:"ˈīdl", picture:"18.png"),
        English(name:"important",cht:"重要的", speak:"imˈpôrtnt", picture:"26.jpg"),
        English(name:"income",cht:"收入", speak:"ˈinˌkəm", picture:"12.jpg"),
        English(name:"increase",cht:"增加", speak:"inˈkrēs", picture:"23.jpg"),
        English(name:"indicate",cht:"指示", speak:"ˈindəˌkāt", picture:"22.jpg"),
        English(name:"informationInternet ",cht:"資訊;訊息", speak:"ˌinfərˈmāSH(ə)n", picture:"21.png"),
        English(name:"Internet",cht:"網際網路", speak:"ˈin(t)ərˌnet", picture:"3.jpg"),
        English(name:"island",cht:"島嶼", speak:"ˈīlənd", picture:"11.jpg"),
        English(name:"illegal",cht:"非法的", speak:"i(l)ˈlēɡəl", picture:"28.jpg"),
        English(name:"incident",cht:"事件", speak:"ˈinsəd(ə)nt", picture:"8.jpg"),
        English(name:"independent",cht:"獨立的", speak:"ˌindəˈpendənt", picture:"27.jpg"),
        English(name:"industry",cht:"工業;行業", speak:"ˈindəstrē", picture:"2.jpg"),
        English(name:"injure",cht:"傷害", speak:"ˈinjər", picture:"16.jpg"),
        English(name:"insect",cht:"昆蟲", speak:"ˈinˌsekt", picture:"24.jpg"),
        English(name:"invent",cht:"發明", speak:"inˈvent", picture:"30.jpg"),
        English(name:"inves",cht:"投資", speak:"inˈvest", picture:"12.jpg"),
        English(name:"investigate",cht:"調查", speak:"inˈvestəˌɡāt", picture:"6.jpg"),
        English(name:"issue",cht:"議題", speak:"ˈiSHo͞o", picture:"20.jpg"),
        English(name:"iron",cht:"鐵", speak:"ī(ə)rn", picture:"17.jpg"),
        English(name:"invite",cht:"邀請", speak:"inˈvīt", picture:"29.jpg"),
        English(name:"invade",cht:"入侵", speak:"inˈvād", picture:"13.png"),
        English(name:"international",cht:"國際的", speak:"ˌin(t)ərˈnaSH(ə)n(ə)l", picture:"10.jpg"),
        English(name:"ice",cht:"冰", speak:"īs", picture:"25.jpg"),
        English(name:"intelligence",cht:"智力", speak:"inˈteləjəns", picture:"15.jpg")
        
        ]
    
    override var prefersStatusBarHidden: Bool{
        return true
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
//        let string = "Hello"
//        let synthesizer = AVSpeechSynthesizer()
//        let voice = AVSpeechSynthesisVoice(language: "en_US")
//        let utterance = AVSpeechUtterance(string: string)
//        utterance.voice = voice
//        synthesizer.speak(utterance)
        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false
        
        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem()
      getWord()
        
    }
    

    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return englishs.count
    }
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath:IndexPath) -> UITableViewCell{
        let cellid = "eng"
        let cell=tableView.dequeueReusableCell(withIdentifier:cellid , for: indexPath) as! engTableViewCell
        cell.namelabel.text=englishs[indexPath.row].name
        cell.wwlabel.text=englishs[indexPath.row].cht
        cell.aalabel.text=englishs[indexPath.row].speak
        cell.pictures.image=UIImage(named: englishs[indexPath.row].picture)
        return cell
    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "englishshow"{
            if let indexPath = tableView.indexPathForSelectedRow{
                let destinationController = segue.destination
                    as! englishViewController
                    destinationController.englishs = englishs[indexPath.row]
                
                
            }
            
            
        }
    }
    //    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    //        let optionMenu = UIAlertController(title:nil,message :"what you want to do",preferredStyle:.actionSheet)
    //        let cancelAction = UIAlertAction(title:"cancel",style:.cancel,handler:nil)
    //        optionMenu.addAction(cancelAction)
    //        self.present(optionMenu,animated: true,completion: nil)
    //        let speakActionHandler={(action:UIAlertAction!)->Void in
    //            let string=self.names["eng"]?[indexPath.row]
    //            let synthesizer=AVSpeechSynthesizer()
    //            let voice=AVSpeechSynthesisVoice(language:"en-US")
    //            let utterance = AVSpeechUtterance(string:string!)
    //            utterance.voice=voice
    //            synthesizer.speak(utterance)
    //        }
    //        let englishWords :String = (self.names["eng"]?[indexPath.row])!
    //        let speakAction=UIAlertAction(title:"Speak Word -> \(englishWords)",style:.default,handler:speakActionHandler)
    //        optionMenu.addAction(speakAction)
    //    }
    
    /* override func tableView(_ tableView:UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath){
     
     if editingStyle == .delete{
     
     names["eng"]?.remove(at: indexPath.row)
     names["cht"]?.remove(at: indexPath.row)
     names["speak"]?.remove(at: indexPath.row)
     names["picture"]?.remove(at: indexPath.row)
     
     
     }
     tableView.reloadData()
     print("Total item: \(names.count)")
     for name in names{
     
     print(name)
     }
     }*/
    
    
    override func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]?
    {
        
        let shareAction = UITableViewRowAction(style: UITableViewRowActionStyle.default, title:"share", handler: {(action,  indexPath) -> Void in
            
            let defaultText = "Just checking in at" + self.englishs[indexPath.row].name
            let activityController = UIActivityViewController(activityItems: [defaultText], applicationActivities: nil)
            self.present(activityController, animated: true, completion: nil)
        })
        
        
        let deleteAction = UITableViewRowAction(style: UITableViewRowActionStyle.default, title:"DELETE")
        {(action, indexPath) -> Void in
            
            self.englishs.remove(at: indexPath.row)
            self.tableView.deleteRows(at: [indexPath], with: .fade)
            
            
            
        }
        let speakAction =  UITableViewRowAction(style: UITableViewRowActionStyle.default, title:"speak")
        {(action, indexPath) -> Void in
            let string=self.englishs[indexPath.row].name
            let synthesizer=AVSpeechSynthesizer()
            let voice=AVSpeechSynthesisVoice(language:"en-US")
            let utterance = AVSpeechUtterance(string:string)
            utterance.voice=voice
            synthesizer.speak(utterance)
            
            
        }
        
        return [deleteAction, shareAction, speakAction]
        
    }
    
    
    /*
     override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
     let cell = tableView.dequeueReusableCell(withIdentifier: "reuseIdentifier", for: indexPath)
     
     // Configure the cell...
     
     return cell
     }
     */
    
    /*
     // Override to support conditional editing of the table view.
     override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
     // Return false if you do not want the specified item to be editable.
     return true
     }
     */
    
    /*
     // Override to support editing the table view.
     override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
     if editingStyle == .delete {
     // Delete the row from the data source
     tableView.deleteRows(at: [indexPath], with: .fade)
     } else if editingStyle == .insert {
     // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
     }
     }
     */
    
    /*
     // Override to support rearranging the table view.
     override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {
     
     }
     */
    
    /*
     // Override to support conditional rearranging of the table view.
     override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
     // Return false if you do not want the item to be re-orderable.
     return true
     }
     */
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
     
}
