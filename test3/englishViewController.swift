//
//  englishViewController.swift
//  test3
//
//  Created by apple on 2017/5/19.
//  Copyright © 2017年 apple. All rights reserved.
//

import UIKit
import AVFoundation

class englishViewController: UIViewController {
    
    @IBOutlet var englishImages : UIImageView!
    @IBOutlet var eng : UILabel!
    @IBOutlet var cht : UILabel!
    @IBOutlet var speak : UILabel!
   
    @IBAction func listen(_ sender: UIButton) {
        let synthesizer=AVSpeechSynthesizer()
        let voice=AVSpeechSynthesisVoice(language:"en-US")
        let utterance = AVSpeechUtterance(string:englishs.name)
        utterance.voice=voice
        synthesizer.speak(utterance)

    }
    var englishs:English!
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        englishImages.image = UIImage(named: englishs.picture)
        self.eng.text = englishs.name
        self.cht.text = englishs.cht
        self.speak.text = englishs.speak
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
